[![pipeline status](https://gitlab.com/YevhenMenshiy/java/badges/master/pipeline.svg)](https://gitlab.com/YevhenMenshiy/java/-/commits/master)
[![coverage report](https://gitlab.com/YevhenMenshiy/java/badges/master/coverage.svg)](https://gitlab.com/YevhenMenshiy/java/-/commits/master)

#Barbershop SpringBoot  project

## Developed using

 - SpringBoot 
 - Spring Security
 - AWS & Minio
 - Docker
 - Postgres
 - Swagger
 - Flyway migrations
 - JDBC


## Required
 - Java 17
 - Gradle 7.3.3
 - Docker
 - Postgres

## Install
 - `git clone https://gitlab.com/YevhenMenshiy/java.git`

## Run
- `./gradlew build` on Linux/Mac or `gradlew.bat` run on Windows to run the app.
- `java -jar build/libs/spdu2022-0.0.1-SNAPSHOT.jar`
### Run in docker container
- `cd ./local`
- run `build.sh` on Linux/Mac or `build.bat` on Windows
- `docker-compose up`

## Application use
 Swagger Api Doc link:
 `http://localhost:8080/swagger-ui/index.html`
- Login: admin; Password: admin