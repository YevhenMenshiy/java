CREATE TABLE users (
	id serial PRIMARY KEY,
	login varchar(100) UNIQUE NOT NULL,
    password varchar(100) NOT NULL,
    enabled boolean NOT NULL
);

CREATE TABLE roles (
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL
);

CREATE TABLE user_roles (
	user_id integer NOT NULL,
	role_id integer NOT NULL
);

ALTER TABLE user_roles ADD CONSTRAINT user_roles_fk0 FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE user_roles ADD CONSTRAINT user_roles_fk1 FOREIGN KEY (role_id) REFERENCES roles(id);