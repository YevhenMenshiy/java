CREATE TABLE companies
(
    id   serial PRIMARY KEY,
    name varchar(150) NOT NULL,
    closed    boolean   NULL,
    created_at  timestamp NOT NULL,
    updated_at  timestamp NULL
);

CREATE TABLE work_time
(
    company_id integer   NOT NULL,
    day        date      NOT NULL,
    open       timestamp NOT NULL,
    close      timestamp NOT NULL
);

CREATE TABLE employers
(
    id         serial PRIMARY KEY,
    company_id integer,
    user_id    integer NOT NULL,
    role_id    integer NOT NULL,
    active     boolean
);

CREATE TABLE masters
(
    id          serial PRIMARY KEY,
    employ_id   integer,
    user_id     integer      NOT NULL,
    description varchar(255) NOT NULL,
    active      boolean,
    created_at  timestamp    NOT NULL,
    updated_at  timestamp    NULL
);

CREATE TABLE calendars
(
    id          serial PRIMARY KEY,
    master_id   integer   NOT NULL,
    offer_begin timestamp NOT NULL,
    offer_end   timestamp NOT NULL
);

CREATE TABLE orders
(
    id          serial PRIMARY KEY,
    user_id     integer   NOT NULL,
    offer_id    integer   NULL,
    calendar_id integer   NOT NULL,
    time        timestamp NOT NULL,
    canceled    boolean   NULL,
    created_at  timestamp NOT NULL,
    updated_at  timestamp NULL
);

CREATE TABLE offers
(
    id        serial PRIMARY KEY,
    master_id integer      NOT NULL,
    name      varchar(255) NOT NULL,
    price     integer      NOT NULL
);

ALTER TABLE employers
    ADD CONSTRAINT employers_fk0 FOREIGN KEY (company_id) REFERENCES companies (id);
ALTER TABLE employers
    ADD CONSTRAINT employ_user_fk0 FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE masters
    ADD CONSTRAINT masters_fk0 FOREIGN KEY (employ_id) REFERENCES employers (id);

ALTER TABLE orders
    ADD CONSTRAINT orders_fk0 FOREIGN KEY (offer_id) REFERENCES offers (id);
ALTER TABLE orders
    ADD CONSTRAINT orders_fk1 FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE orders
    ADD CONSTRAINT orders_fk2 FOREIGN KEY (calendar_id) REFERENCES calendars (id);

ALTER TABLE work_time
    ADD CONSTRAINT work_time_fk0 FOREIGN KEY (company_id) REFERENCES companies (id);

ALTER TABLE calendars
    ADD CONSTRAINT calendars_fk0 FOREIGN KEY (master_id) REFERENCES masters (id);

ALTER TABLE offers
    ADD CONSTRAINT offers_fk0 FOREIGN KEY (master_id) REFERENCES masters (id);
