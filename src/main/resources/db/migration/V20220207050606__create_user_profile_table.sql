CREATE TABLE user_profile (
	user_id integer NOT NULL,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	profile_image text
);
ALTER TABLE user_profile ADD CONSTRAINT user_profile_fk0 FOREIGN KEY (user_id) REFERENCES users(id);
