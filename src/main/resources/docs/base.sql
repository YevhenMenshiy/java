CREATE TABLE companies (
	id serial NOT NULL,
	name varchar(150) NOT NULL,
	CONSTRAINT companies_pk PRIMARY KEY (id)
);

CREATE TABLE employers (
	id serial NOT NULL,
	company_id serial NOT NULL,
	user_id serial NOT NULL,
	CONSTRAINT employers_pk PRIMARY KEY (id)
);

CREATE TABLE users (
	id serial NOT NULL,
	login varchar(100) UNIQUE NOT NULL,
    password varchar(100) NOT NULL,
	CONSTRAINT users_pk PRIMARY KEY (id)
);

CREATE TABLE user_profile (
	user_id serial NOT NULL,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL
);

CREATE TABLE masters (
	id serial NOT NULL,
	employ_id serial NOT NULL,
	description varchar(255) NOT NULL,
	CONSTRAINT masters_pk PRIMARY KEY (id)

);

CREATE TABLE work_time (
	company_id serial NOT NULL,
	day_of_week varchar(2) NOT NULL,
	open TIME NOT NULL,
	close TIME NOT NULL,
	CONSTRAINT work_time_pk PRIMARY KEY (company_id)
);

CREATE TABLE calendar (
	id serial NOT NULL,
	master_id serial NOT NULL,
	week_of_year integer(2) NOT NULL,
	day_of_week varchar(2) NOT NULL,
	start TIME NOT NULL,
	end TIME NOT NULL,
	CONSTRAINT calendar_pk PRIMARY KEY (id)
);

CREATE TABLE orders (
	id serial NOT NULL,
	offer_id serial NOT NULL,
	client_user_id serial NOT NULL,
	calendar_id serial NOT NULL,
	time TIME NOT NULL,
	CONSTRAINT orders_pk PRIMARY KEY (id)
);

CREATE TABLE offers (
	id serial NOT NULL,
	master_id serial NOT NULL,
	name varchar(255) NOT NULL,
	price integer NOT NULL,
	CONSTRAINT offers_pk PRIMARY KEY (id)

);

CREATE TABLE roles (
	id serial NOT NULL,
	name varchar(50) NOT NULL,
	CONSTRAINT roles_pk PRIMARY KEY (id)
);

CREATE TABLE user_roles (
	user_id serial NOT NULL,
	role_id serial NOT NULL
);

CREATE TABLE permissions (
	id serial NOT NULL,
	name varchar(100) NOT NULL,
	CONSTRAINT permissions_pk PRIMARY KEY (id)
);

CREATE TABLE roles_permissions (
	role_id serial NOT NULL,
	permission_id serial NOT NULL
);


CREATE TABLE settings (
	id serial NOT NULL,
	settings json NOT NULL,
	CONSTRAINT settings_pk PRIMARY KEY (id)
);

CREATE TABLE images (
	id serial NOT NULL,
	related_table varchar(100) NOT NULL,
	related_id serial NOT NULL,
	src TEXT NOT NULL,
	CONSTRAINT images_pk PRIMARY KEY (id)
);


ALTER TABLE employers ADD CONSTRAINT employers_fk0 FOREIGN KEY (company_id) REFERENCES companies(id);


ALTER TABLE employ ADD CONSTRAINT employ_user_fk0 FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE masters ADD CONSTRAINT masters_fk0 FOREIGN KEY (employ_id) REFERENCES employers(id);

ALTER TABLE user_roles ADD CONSTRAINT user_roles_fk0 FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE user_roles ADD CONSTRAINT user_roles_fk1 FOREIGN KEY (role_id) REFERENCES roles(id);


ALTER TABLE roles_permissions ADD CONSTRAINT roles_permissions_fk0 FOREIGN KEY (role_id) REFERENCES roles(id);
ALTER TABLE roles_permissions ADD CONSTRAINT roles_permissions_fk1 FOREIGN KEY (permission_id) REFERENCES permissions(id);

ALTER TABLE user_profile ADD CONSTRAINT user_profile_fk0 FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE orders ADD CONSTRAINT orders_fk0 FOREIGN KEY (offer_id) REFERENCES offers(id);
ALTER TABLE orders ADD CONSTRAINT orders_fk1 FOREIGN KEY (client_user_id) REFERENCES users(id);
ALTER TABLE orders ADD CONSTRAINT orders_fk2 FOREIGN KEY (calendar_id) REFERENCES calendar(id);

ALTER TABLE work_time ADD CONSTRAINT work_time_fk0 FOREIGN KEY (company_id) REFERENCES companies(id);

ALTER TABLE calendar ADD CONSTRAINT calendar_fk0 FOREIGN KEY (master_id) REFERENCES masters(id);

ALTER TABLE offers ADD CONSTRAINT offers_fk0 FOREIGN KEY (master_id) REFERENCES masters(id);











