package org.spd.masters.offer.crud.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.spd.masters.entity.Offer;
import org.spd.masters.util.DbEntityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcOfferRepository implements OfferRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcOfferRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Integer insert(Offer offer) {
        SqlParameterSource toOffer =
                new MapSqlParameterSource()
                        .addValue("master_id", offer.getMaster_id())
                        .addValue("name", offer.getName())
                        .addValue("price", offer.getPrice());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO offers (master_id, name, price) " +
                "VALUES (:master_id, :name, :price)";

        try {
            jdbcTemplate.update(sql, toOffer, keyHolder);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        final Map<String, Object> map = keyHolder.getKeys();

        if (map == null) {
            throw new RuntimeException("Offer not inserted to database!");
        }

        return (Integer) map.get("id");
    }

    public Offer save(Offer offer) {
        int offerId = insert(offer);
        offer.setId(offerId);
        return offer;
    }

    @Override
    public List<Offer> findAll() {
        return jdbcTemplate
                .query("SELECT * FROM offers",
                        (resultSet, i) -> DbEntityUtil.getOffer(resultSet)
                );
    }

    @Override
    public Optional<Offer> findById(Integer id) {
        try {
            Offer offer = (Offer) jdbcTemplate
                    .queryForObject("SELECT * FROM offers WHERE id = :id",
                            new MapSqlParameterSource("id", id),
                            (resultSet, i) -> DbEntityUtil.getOffer(resultSet)
                    );
            return Optional.ofNullable(offer);
        } catch (EmptyResultDataAccessException exception) {
            return Optional.empty();
        }
    }

    @Override
    public void deleteById(Integer id) {
        jdbcTemplate
                .update("DELETE FROM offers WHERE id = :id",
                        new MapSqlParameterSource("id", id)
                );
    }

    @Override
    public int updateById(Integer id, Offer offer) {
        SqlParameterSource toOffer =
                new MapSqlParameterSource("id", id)
                        .addValue("master_id", offer.getMaster_id())
                        .addValue("name", offer.getName())
                        .addValue("price", offer.getPrice());

        String sql = "UPDATE offers SET " +
                "master_id = :master_id, name = :name, price = :price " +
                "WHERE id = :id";
        return jdbcTemplate.update(sql, toOffer);
    }

    @Override
    public void deleteAll() {
        jdbcTemplate.update("DELETE FROM offers",
                new MapSqlParameterSource());
    }

}
