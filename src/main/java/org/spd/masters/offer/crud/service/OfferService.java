package org.spd.masters.offer.crud.service;

import java.util.List;
import org.spd.masters.entity.Offer;

public interface OfferService {
    Offer findById(Integer offerId);

    List<Offer> get();

    Offer create(Offer offer);

    void update(Integer offerId, Offer offer);

    void remove(Integer offerId);

    void remove();
}
