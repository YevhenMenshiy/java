package org.spd.masters.offer.crud.repository;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Offer;

public interface OfferRepository {

    Integer insert(Offer offer);

    Offer save(Offer offer);

    List<Offer> findAll();

    Optional<Offer> findById(Integer id);

    void deleteById(Integer id);

    int updateById(Integer id, Offer offer);

    void deleteAll();
}
