package org.spd.masters.offer.crud.service;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Offer;
import org.spd.masters.exceptions.BadRequestException;
import org.spd.masters.exceptions.NotFoundException;
import org.spd.masters.offer.crud.repository.OfferRepository;
import org.spd.masters.resource.StringResoures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersistentOfferService implements OfferService {

    public static final int DONE = 1;

    private final OfferRepository offerRepository;

    @Autowired
    public PersistentOfferService(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @Override
    public Offer findById(Integer offerId) {
        Optional<Offer> offer = offerRepository.findById(offerId);

        if (offer.isEmpty()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }
        return offer.get();
    }

    @Override
    public List<Offer> get() {

        var offers = offerRepository.findAll();
        if (offers.isEmpty()) {
            throw new NotFoundException(StringResoures.NO_ORDERS);
        }
        return offers;
    }

    @Override
    public Offer create(Offer offer) {
        try {
            return offerRepository.save(offer);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public void update(Integer offerId, Offer offer) {
        Optional<Offer> existingOffer = offerRepository.findById(offerId);

        if (existingOffer.isEmpty()) {
            throw new NotFoundException(StringResoures.ORDER_NOT_PRESENT);
        }

        var updated = offerRepository.updateById(existingOffer.get().getId(), offer);

        if (updated != DONE) {
            throw new BadRequestException(StringResoures.NOT_UPDATED);
        }

    }

    @Override
    public void remove(Integer offerId) {
        Optional<Offer> offer = offerRepository.findById(offerId);

        if (offer.isEmpty()) {
            throw new NotFoundException(StringResoures.ORDER_NOT_PRESENT);
        }
        offerRepository.deleteById(offerId);

    }

    @Override
    public void remove() {
        offerRepository.deleteAll();
    }
}
