package org.spd.masters.offer.crud.web.dto;

import java.time.LocalDateTime;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class OfferDTO {

    @NotNull(message = "Master id can`t be empty")
    Integer master_id;

    @NotNull(message = "Offer name can`t be empty")
    String name;

    @NotNull(message = "Price can`t be empty")
    @Min(1)
    Integer price;

    public Integer getMaster_id() {
        return master_id;
    }

    public void setMaster_id(Integer master_id) {
        this.master_id = master_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
