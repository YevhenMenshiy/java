package org.spd.masters.offer.crud.web.controller;

import javax.validation.Valid;
import org.spd.masters.entity.Offer;
import org.spd.masters.offer.crud.service.PersistentOfferService;
import org.spd.masters.offer.crud.web.dto.OfferDTO;
import org.spd.masters.resource.StringResoures;
import org.spd.masters.util.CustomResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/crud/offers")
public class OfferCRUDController {

    private final PersistentOfferService persistentOfferService;

    @Autowired
    public OfferCRUDController(PersistentOfferService persistentOfferService) {
        this.persistentOfferService = persistentOfferService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CustomResponse getAll() {
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK,
                persistentOfferService.get());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CustomResponse create(@Valid @RequestBody OfferDTO dto) {
        var saved = persistentOfferService.create(toEntity(dto));
        return new CustomResponse(StringResoures.CREATED_SUCCESSFUL, HttpStatus.CREATED, saved);
    }

    @GetMapping("/{offerId}")
    public CustomResponse getById(@PathVariable Integer offerId) {
        var response = persistentOfferService.findById(offerId);
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK, response);
    }

    @PutMapping("/{offerId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse update(@PathVariable Integer offerId, @RequestBody OfferDTO dto) {
        var saved = toEntity(dto);
        persistentOfferService.update(offerId, toEntity(dto));
        return new CustomResponse(StringResoures.UPDATED_SUCCESSFUL, HttpStatus.CREATED, saved);
    }

    @DeleteMapping("/{offerId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteById(@PathVariable Integer offerId) {
        persistentOfferService.remove(offerId);
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteAll() {
        persistentOfferService.remove();
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    private Offer toEntity(OfferDTO dto) {
        Offer offer = new Offer();
        BeanUtils.copyProperties(dto, offer);
        return offer;
    }
}
