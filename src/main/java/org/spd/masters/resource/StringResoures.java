package org.spd.masters.resource;

public class StringResoures {
    public static final String NO_CALENDARS = "No working days!";
    public static final String NO_ORDERS = "No orders found!";
    public static final String NO_SLOTS = "No slots found!";
    public static final String NOT_PRESENT = "Not found!";
    public static final String SUCCESSFUL = "Success!";
    public static final String ORDER_CREATED = "Order created successful!";
    public static final String BAD_DATE = "Date is not valid!";
    public static final String DELETED_SUCCESSFUL = "Deleted!";
    public static final String ORDER_NOT_PRESENT = "Order not found!";
    public static final String UPDATED = "Updated!";
    public static final String NOT_UPDATED = "Not updated!";
    public static final String ERROR = "Error!";
    public static final String IS_ALLREADY_TAKEN = "Time is not available, it is taken!";
    public static final String ORDER_UPDATED = "Order updated!";
    public static final String CALENDAR_NOT_PRESENT = "Calendar not found!";
    public static final String CREATED_SUCCESSFUL = "Created successful!";
    public static final String UPDATED_SUCCESSFUL = "Updated successful!";
}
