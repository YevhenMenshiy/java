package org.spd.masters.master.crud.service;

import java.util.List;
import org.spd.masters.entity.Master;

public interface MasterService {
    Master findById(Integer masterId);

    List<Master> get();

    Master create(Master master);

    void update(Integer masterId, Master master);

    void remove(Integer masterId);

    void remove();
}
