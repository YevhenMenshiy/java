package org.spd.masters.master.crud.service;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Master;
import org.spd.masters.exceptions.BadRequestException;
import org.spd.masters.exceptions.NotFoundException;
import org.spd.masters.master.crud.repository.MasterRepository;
import org.spd.masters.resource.StringResoures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersistentMasterService implements MasterService {

    public static final int DONE = 1;

    private final MasterRepository masterRepository;

    @Autowired
    public PersistentMasterService(MasterRepository masterRepository) {
        this.masterRepository = masterRepository;
    }

    @Override
    public Master findById(Integer masterId) {
        Optional<Master> master = masterRepository.findById(masterId);

        if (master.isEmpty()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }
        return master.get();
    }

    @Override
    public List<Master> get() {

        var masters = masterRepository.findAll();
        if (masters.isEmpty()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }
        return masters;
    }

    @Override
    public Master create(Master master) {
        try {
            return masterRepository.save(master);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public void update(Integer masterId, Master master) {
        Optional<Master> existingMaster = masterRepository.findById(masterId);

        if (existingMaster.isEmpty()) {
            throw new NotFoundException(StringResoures.CALENDAR_NOT_PRESENT);
        }

        var updated = masterRepository.updateById(existingMaster.get().getId(), master);

        if (updated != DONE) {
            throw new BadRequestException(StringResoures.NOT_UPDATED);
        }

    }

    @Override
    public void remove(Integer masterId) {
        Optional<Master> master = masterRepository.findById(masterId);

        if (master.isEmpty()) {
            throw new NotFoundException(StringResoures.CALENDAR_NOT_PRESENT);
        }
        masterRepository.deleteById(masterId);

    }

    @Override
    public void remove() {
        masterRepository.deleteAll();
    }
}
