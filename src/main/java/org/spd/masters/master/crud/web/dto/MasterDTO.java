package org.spd.masters.master.crud.web.dto;

import javax.validation.constraints.NotNull;

public class MasterDTO {

    Integer employ_id;

    @NotNull(message = "User id can`t be empty")
    Integer user_id;

    @NotNull(message = "Description can`t be empty")
    String description;

    Boolean active;

    public Integer getEmploy_id() {
        return employ_id;
    }

    public void setEmploy_id(Integer employ_id) {
        this.employ_id = employ_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
