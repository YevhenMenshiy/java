package org.spd.masters.master.crud.web.controller;

import javax.validation.Valid;
import org.spd.masters.entity.Master;
import org.spd.masters.master.crud.service.MasterService;
import org.spd.masters.master.crud.web.dto.MasterDTO;
import org.spd.masters.resource.StringResoures;
import org.spd.masters.util.CustomResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/crud/masters")
public class MasterCRUDController {

    private final MasterService masterService;

    @Autowired
    public MasterCRUDController(MasterService masterService) {
        this.masterService = masterService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CustomResponse getAll() {
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK, masterService.get());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CustomResponse create(@Valid @RequestBody MasterDTO dto) {
        var saved = masterService.create(toEntity(dto));
        return new CustomResponse(StringResoures.CREATED_SUCCESSFUL, HttpStatus.CREATED, saved);
    }

    @GetMapping("/{masterId}")
    public CustomResponse getById(@PathVariable Integer masterId) {
        var response = masterService.findById(masterId);
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK, response);
    }

    @PutMapping("/{masterId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse update(@PathVariable Integer masterId, @RequestBody MasterDTO dto) {
        var saved = toEntity(dto);
        masterService.update(masterId, toEntity(dto));
        return new CustomResponse(StringResoures.UPDATED_SUCCESSFUL, HttpStatus.CREATED, saved);
    }

    @DeleteMapping("/{masterId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteById(@PathVariable Integer masterId) {
        masterService.remove(masterId);
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteAll() {
        masterService.remove();
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    private Master toEntity(MasterDTO dto) {
        Master master = new Master();
        BeanUtils.copyProperties(dto, master);
        return master;
    }
}
