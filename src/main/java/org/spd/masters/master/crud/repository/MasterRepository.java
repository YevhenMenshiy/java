package org.spd.masters.master.crud.repository;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Master;

public interface MasterRepository {

    Integer insert(Master master);

    Master save(Master master);

    List<Master> findAll();

    Optional<Master> findById(Integer id);

    void deleteById(Integer id);

    int updateById(Integer id, Master master);

    void deleteAll();
}
