package org.spd.masters.master.crud.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.spd.masters.entity.Master;
import org.spd.masters.util.DbEntityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcMasterRepository implements MasterRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcMasterRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Integer insert(Master master) {

        SqlParameterSource toMaster =
                new MapSqlParameterSource()
                        .addValue("employ_id", master.getEmploy_id())
                        .addValue("user_id", master.getUser_id())
                        .addValue("description", master.getDescription())
                        .addValue("active", master.getActive());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO masters (employ_id, user_id, description, active, created_at) " +
                "VALUES (:employ_id, :user_id, :description, :active, NOW())";

        try {
            jdbcTemplate.update(sql, toMaster, keyHolder);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        final Map<String, Object> map = keyHolder.getKeys();

        if (map == null) {
            throw new RuntimeException("Master not inserted to database!");
        }

        return (Integer) map.get("id");
    }

    public Master save(Master master) {
        int masterId = insert(master);
        master.setId(masterId);
        return master;
    }

    @Override
    public List<Master> findAll() {
        return jdbcTemplate
                .query("SELECT * FROM masters",
                        (resultSet, i) -> DbEntityUtil.getMaster(resultSet)
                );
    }

    @Override
    public Optional<Master> findById(Integer id) {
        try {
            Master master = (Master) jdbcTemplate
                    .queryForObject("SELECT * FROM masters WHERE id = :id",
                            new MapSqlParameterSource("id", id),
                            (resultSet, i) -> DbEntityUtil.getMaster(resultSet)
                    );
            return Optional.ofNullable(master);
        } catch (EmptyResultDataAccessException exception) {
            return Optional.empty();
        }
    }

    @Override
    public void deleteById(Integer id) {
        jdbcTemplate
                .update("DELETE FROM masters WHERE id = :id",
                        new MapSqlParameterSource("id", id)
                );
    }

    @Override
    public int updateById(Integer id, Master master) {
        SqlParameterSource toMaster =
                new MapSqlParameterSource("id", id)
                        .addValue("employ_id", master.getEmploy_id())
                        .addValue("user_id", master.getUser_id())
                        .addValue("description", master.getDescription())
                        .addValue("active", master.getActive());

        String sql = "UPDATE masters SET " +
                "employ_id = :employ_id, user_id = :user_id, description = :description," +
                "active = :active, updated_at = NOW() WHERE id = :id";
        return jdbcTemplate.update(sql, toMaster);
    }

    @Override
    public void deleteAll() {
        jdbcTemplate.update("DELETE FROM masters",
                new MapSqlParameterSource());
    }

}
