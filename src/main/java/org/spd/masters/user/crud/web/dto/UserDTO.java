package org.spd.masters.user.crud.web.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class UserDTO {

    @NotNull(message = "Login id can`t be empty")
    @Min(3)
    String login;

    @NotNull(message = "Password name can`t be empty")
    @Min(6)
    String password;

    Boolean enabled;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
