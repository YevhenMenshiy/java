package org.spd.masters.user.crud.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.spd.masters.entity.User;
import org.spd.masters.util.DbEntityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcUserRepository implements UserRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcUserRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Integer insert(User user) {
        SqlParameterSource toUser =
                new MapSqlParameterSource()
                        .addValue("login", user.getLogin())
                        .addValue("password", user.getPassword())
                        .addValue("enabled", user.getEnabled());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO users (login, password, enabled) " +
                "VALUES (:login, :password, :enabled)";

        try {
            jdbcTemplate.update(sql, toUser, keyHolder);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        final Map<String, Object> map = keyHolder.getKeys();

        if (map == null) {
            throw new RuntimeException("User not inserted to database!");
        }

        return (Integer) map.get("id");
    }

    public User save(User user) {
        int userId = insert(user);
        user.setId(userId);
        return user;
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate
                .query("SELECT * FROM users",
                        (resultSet, i) -> DbEntityUtil.getUser(resultSet)
                );
    }

    @Override
    public Optional<User> findById(Integer id) {
        try {
            User user = (User) jdbcTemplate
                    .queryForObject("SELECT * FROM users WHERE id = :id",
                            new MapSqlParameterSource("id", id),
                            (resultSet, i) -> DbEntityUtil.getUser(resultSet)
                    );
            return Optional.ofNullable(user);
        } catch (EmptyResultDataAccessException exception) {
            return Optional.empty();
        }
    }

    @Override
    public void deleteById(Integer id) {
        jdbcTemplate
                .update("DELETE FROM users WHERE id = :id",
                        new MapSqlParameterSource("id", id)
                );
    }

    @Override
    public int updateById(Integer id, User user) {
        SqlParameterSource toUser =
                new MapSqlParameterSource("id", id)
                        .addValue("login", user.getLogin())
                        .addValue("password", user.getPassword())
                        .addValue("enabled", user.getEnabled());

        String sql = "UPDATE users SET " +
                "login = :login, password = :password, enabled = :enabled " +
                "WHERE id = :id";
        return jdbcTemplate.update(sql, toUser);
    }

    @Override
    public void deleteAll() {
        jdbcTemplate.update("DELETE FROM users",
                new MapSqlParameterSource());
    }

}
