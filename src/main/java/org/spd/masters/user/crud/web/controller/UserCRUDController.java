package org.spd.masters.user.crud.web.controller;

import javax.validation.Valid;
import org.spd.masters.entity.User;
import org.spd.masters.resource.StringResoures;
import org.spd.masters.user.crud.service.PersistentUserService;
import org.spd.masters.user.crud.web.dto.UserDTO;
import org.spd.masters.util.CustomResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/crud/users")
public class UserCRUDController {

    private final PersistentUserService persistentUserService;

    @Autowired
    public UserCRUDController(PersistentUserService persistentUserService) {
        this.persistentUserService = persistentUserService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CustomResponse getAll() {
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK,
                persistentUserService.get());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CustomResponse create(@Valid @RequestBody UserDTO dto) {
        var saved = persistentUserService.create(toEntity(dto));
        return new CustomResponse(StringResoures.CREATED_SUCCESSFUL, HttpStatus.CREATED, saved);
    }

    @GetMapping("/{userId}")
    public CustomResponse getById(@PathVariable Integer userId) {
        var response = persistentUserService.findById(userId);
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK, response);
    }

    @PutMapping("/{userId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse update(@PathVariable Integer userId, @RequestBody UserDTO dto) {
        var saved = toEntity(dto);
        persistentUserService.update(userId, toEntity(dto));
        return new CustomResponse(StringResoures.UPDATED_SUCCESSFUL, HttpStatus.CREATED, saved);
    }

    @DeleteMapping("/{userId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteById(@PathVariable Integer userId) {
        persistentUserService.remove(userId);
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteAll() {
        persistentUserService.remove();
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    private User toEntity(UserDTO dto) {
        return new User(
                dto.getLogin(),
                dto.getPassword(),
                dto.getEnabled(),
                true,
                true,
                true,
                null);
    }
}
