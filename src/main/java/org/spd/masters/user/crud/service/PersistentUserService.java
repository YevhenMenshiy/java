package org.spd.masters.user.crud.service;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.User;
import org.spd.masters.exceptions.BadRequestException;
import org.spd.masters.exceptions.NotFoundException;
import org.spd.masters.resource.StringResoures;
import org.spd.masters.user.crud.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersistentUserService implements UserService {

    public static final int DONE = 1;

    private final UserRepository userRepository;

    @Autowired
    public PersistentUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findById(Integer userId) {
        Optional<User> user = userRepository.findById(userId);

        if (user.isEmpty()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }
        return user.get();
    }

    @Override
    public List<User> get() {

        var users = userRepository.findAll();
        if (users.isEmpty()) {
            throw new NotFoundException(StringResoures.NO_ORDERS);
        }
        return users;
    }

    @Override
    public User create(User user) {
        try {
            return userRepository.save(user);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public void update(Integer userId, User user) {
        Optional<User> existingUser = userRepository.findById(userId);

        if (existingUser.isEmpty()) {
            throw new NotFoundException(StringResoures.ORDER_NOT_PRESENT);
        }

        var updated = userRepository.updateById(existingUser.get().getId(), user);

        if (updated != DONE) {
            throw new BadRequestException(StringResoures.NOT_UPDATED);
        }

    }

    @Override
    public void remove(Integer userId) {
        Optional<User> user = userRepository.findById(userId);

        if (user.isEmpty()) {
            throw new NotFoundException(StringResoures.ORDER_NOT_PRESENT);
        }
        userRepository.deleteById(userId);

    }

    @Override
    public void remove() {
        userRepository.deleteAll();
    }
}
