package org.spd.masters.user.crud.repository;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.User;

public interface UserRepository {

    Integer insert(User user);

    User save(User user);

    List<User> findAll();

    Optional<User> findById(Integer id);

    void deleteById(Integer id);

    int updateById(Integer id, User user);

    void deleteAll();
}
