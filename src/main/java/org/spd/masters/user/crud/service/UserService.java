package org.spd.masters.user.crud.service;

import java.util.List;
import org.spd.masters.entity.User;

public interface UserService {
    User findById(Integer userId);

    List<User> get();

    User create(User user);

    void update(Integer userId, User user);

    void remove(Integer userId);

    void remove();
}
