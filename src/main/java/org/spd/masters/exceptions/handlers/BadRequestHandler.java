package org.spd.masters.exceptions.handlers;

import javax.servlet.http.HttpServletRequest;
import org.spd.masters.exceptions.BadRequestException;
import org.spd.masters.util.CustomResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BadRequestHandler {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> handleNotFoundException(HttpServletRequest request, Exception ex) {

        return new ResponseEntity<>(
                new CustomResponse("Error", HttpStatus.BAD_REQUEST, ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }
}