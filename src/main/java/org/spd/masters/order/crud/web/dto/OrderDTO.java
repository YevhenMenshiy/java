package org.spd.masters.order.crud.web.dto;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;

public class OrderDTO {

    @NotNull(message = "Client user id can`t be empty")
    Integer user_id;

    Integer offer_id;

    @NotNull(message = "Calendar entity id can`t be empty")
    Integer calendar_id;

    @NotNull(message = "Ordered datetime can`t be empty")
    LocalDateTime time;

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(Integer offer_id) {
        this.offer_id = offer_id;
    }

    public Integer getCalendar_id() {
        return calendar_id;
    }

    public void setCalendar_id(Integer calendar_id) {
        this.calendar_id = calendar_id;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public LocalDateTime getTime() {
        return time;
    }

}
