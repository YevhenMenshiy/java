package org.spd.masters.order.crud.web.controller;

import javax.validation.Valid;
import org.spd.masters.entity.Order;
import org.spd.masters.order.crud.service.PersistentOrderService;
import org.spd.masters.order.crud.web.dto.OrderDTO;
import org.spd.masters.resource.StringResoures;
import org.spd.masters.util.CustomResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/crud/orders")
public class OrderCRUDController {

    private final PersistentOrderService persistentOrderService;

    @Autowired
    public OrderCRUDController(PersistentOrderService persistentOrderService) {
        this.persistentOrderService = persistentOrderService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CustomResponse getAll() {
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK,
                persistentOrderService.get());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CustomResponse create(@Valid @RequestBody OrderDTO dto) {
        var saved = persistentOrderService.create(toEntity(dto));
        return new CustomResponse(StringResoures.ORDER_CREATED, HttpStatus.CREATED, saved);
    }

    @GetMapping("/{orderId}")
    public CustomResponse getById(@PathVariable Integer orderId) {
        var response = persistentOrderService.findById(orderId);
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK, response);
    }

    @PutMapping("/{orderId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse update(@PathVariable Integer orderId, @RequestBody OrderDTO dto) {
        var saved = toEntity(dto);
        persistentOrderService.update(orderId, toEntity(dto));
        return new CustomResponse(StringResoures.ORDER_UPDATED, HttpStatus.CREATED, saved);
    }

    @DeleteMapping("/{orderId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteById(@PathVariable Integer orderId) {
        persistentOrderService.remove(orderId);
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteAll() {
        persistentOrderService.remove();
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    private Order toEntity(OrderDTO dto) {
        Order order = new Order();
        BeanUtils.copyProperties(dto, order);
        return order;
    }
}
