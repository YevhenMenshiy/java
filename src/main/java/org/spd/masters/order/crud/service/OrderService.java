package org.spd.masters.order.crud.service;

import java.util.List;
import org.spd.masters.entity.Order;

public interface OrderService {
    Order findById(Integer orderId);

    List<Order> get();

    Order create(Order order);

    void update(Integer orderId, Order order);

    void remove(Integer orderId);

    void remove();
}
