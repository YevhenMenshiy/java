package org.spd.masters.order.crud.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.spd.masters.entity.Order;
import org.spd.masters.exceptions.BadRequestException;
import org.spd.masters.order.crud.repository.OrderRepository;
import org.spd.masters.resource.StringResoures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderValidator {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderValidator(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void validate(Order order) {
        if (order.getTime().toLocalDate().isBefore(LocalDate.now())) {
            throw new BadRequestException(StringResoures.BAD_DATE);
        }
        List<Order> orders = orderRepository.findAll();
        if (ordersContainsTimeDate(orders, order.getTime())) {
            throw new BadRequestException(StringResoures.IS_ALLREADY_TAKEN);
        }
    }

    private boolean ordersContainsTimeDate(final List<Order> list, final LocalDateTime time) {
        return list.stream().map(Order::getTime).anyMatch(time::equals);
    }
}
