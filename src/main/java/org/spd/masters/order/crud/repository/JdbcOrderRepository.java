package org.spd.masters.order.crud.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.spd.masters.entity.Order;
import org.spd.masters.util.DbEntityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcOrderRepository implements OrderRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcOrderRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Integer insert(Order order) {
        SqlParameterSource toOrder =
                new MapSqlParameterSource()
                        .addValue("user_id", order.getUser_id())
                        .addValue("offer_id", order.getOffer_id())
                        .addValue("calendar_id", order.getCalendar_id())
                        .addValue("time", order.getTime());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO orders (user_id, offer_id, calendar_id, time, created_at) " +
                "VALUES (:user_id, :offer_id, :calendar_id, :time, NOW())";

        try {
            jdbcTemplate.update(sql, toOrder, keyHolder);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        final Map<String, Object> map = keyHolder.getKeys();

        if (map == null) {
            throw new RuntimeException("Order not inserted to database!");
        }

        return (Integer) map.get("id");
    }

    public Order save(Order order) {
        int orderId = insert(order);
        order.setId(orderId);
        return order;
    }

    @Override
    public List<Order> findAll() {
        return jdbcTemplate
                .query("SELECT * FROM orders",
                        (resultSet, i) -> DbEntityUtil.getOrder(resultSet)
                );
    }

    @Override
    public Optional<Order> findById(Integer id) {
        try {
            Order order = (Order) jdbcTemplate
                    .queryForObject("SELECT * FROM orders WHERE id = :id",
                            new MapSqlParameterSource("id", id),
                            (resultSet, i) -> DbEntityUtil.getOrder(resultSet)
                    );
            return Optional.ofNullable(order);
        } catch (EmptyResultDataAccessException exception) {
            return Optional.empty();
        }
    }

    @Override
    public void deleteById(Integer id) {
        jdbcTemplate
                .update("DELETE FROM orders WHERE id = :id",
                        new MapSqlParameterSource("id", id)
                );
    }

    @Override
    public int updateById(Integer id, Order order) {
        SqlParameterSource toOrder =
                new MapSqlParameterSource("id", id)
                        .addValue("user_id", order.getUser_id())
                        .addValue("offer_id", order.getOffer_id())
                        .addValue("calendar_id", order.getCalendar_id())
                        .addValue("time", order.getTime());

        String sql = "UPDATE orders SET " +
                "user_id = :user_id, offer_id = :offer_id, calendar_id = :calendar_id, time = :time, updated_at = NOW() " +
                "WHERE id = :id";
        return jdbcTemplate.update(sql, toOrder);
    }

    @Override
    public void deleteAll() {
        jdbcTemplate.update("DELETE FROM orders",
                new MapSqlParameterSource());
    }

}
