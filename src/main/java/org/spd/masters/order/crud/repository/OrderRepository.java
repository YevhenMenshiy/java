package org.spd.masters.order.crud.repository;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Order;

public interface OrderRepository {

    Integer insert(Order order);

    Order save(Order order);

    List<Order> findAll();

    Optional<Order> findById(Integer id);

    void deleteById(Integer id);

    int updateById(Integer id, Order order);

    void deleteAll();
}
