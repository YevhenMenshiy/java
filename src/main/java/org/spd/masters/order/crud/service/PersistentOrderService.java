package org.spd.masters.order.crud.service;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Order;
import org.spd.masters.exceptions.BadRequestException;
import org.spd.masters.exceptions.NotFoundException;
import org.spd.masters.order.crud.repository.OrderRepository;
import org.spd.masters.resource.StringResoures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersistentOrderService implements OrderService {

    public static final int DONE = 1;

    private final OrderRepository orderRepository;
    private final OrderValidator orderValidator;

    @Autowired
    public PersistentOrderService(OrderRepository orderRepository,
                                  OrderValidator orderValidator) {
        this.orderRepository = orderRepository;
        this.orderValidator = orderValidator;
    }

    @Override
    public Order findById(Integer orderId) {
        Optional<Order> order = orderRepository.findById(orderId);

        if (order.isEmpty()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }
        return order.get();
    }

    @Override
    public List<Order> get() {

        var orders = orderRepository.findAll();
        if (orders.isEmpty()) {
            throw new NotFoundException(StringResoures.NO_ORDERS);
        }
        return orders;
    }

    @Override
    public Order create(Order order) {

        orderValidator.validate(order);
        try {
            return orderRepository.save(order);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public void update(Integer orderId, Order order) {
        Optional<Order> existingOrder = orderRepository.findById(orderId);

        if (existingOrder.isEmpty()) {
            throw new NotFoundException(StringResoures.ORDER_NOT_PRESENT);
        }

        orderValidator.validate(order);

        var updated = orderRepository.updateById(existingOrder.get().getId(), order);

        if (updated != DONE) {
            throw new BadRequestException(StringResoures.NOT_UPDATED);
        }

    }

    @Override
    public void remove(Integer orderId) {
        Optional<Order> order = orderRepository.findById(orderId);

        if (order.isEmpty()) {
            throw new NotFoundException(StringResoures.ORDER_NOT_PRESENT);
        }
        orderRepository.deleteById(orderId);

    }

    @Override
    public void remove() {
        orderRepository.deleteAll();
    }
}
