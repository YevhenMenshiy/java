package org.spd.masters.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Map;
import org.springframework.http.HttpStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomResponse {

    private String message;
    private HttpStatus status;
    private Object response;
    private Map<String, String> errors;

    public CustomResponse() {
    }

    public CustomResponse(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    public CustomResponse(String message, HttpStatus status, Map<String, String> errors) {
        this.message = message;
        this.status = status;
        this.errors = errors;
    }

    public CustomResponse(String message, HttpStatus status, Object object) {
        this.message = message;
        this.status = status;
        this.response = object;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }
}