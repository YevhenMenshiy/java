package org.spd.masters.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.spd.masters.entity.Calendar;
import org.spd.masters.entity.Master;
import org.spd.masters.entity.Offer;
import org.spd.masters.entity.Order;
import org.spd.masters.entity.User;
import org.spd.masters.exceptions.NotFoundException;
import org.spd.masters.resource.StringResoures;

public class DbEntityUtil {

    public static Order getOrder(ResultSet resultSet) throws SQLException {
        if (!resultSet.next()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }

        Order dbOrder = new Order();
        dbOrder.setId(resultSet.getInt("id"));
        dbOrder.setUser_id(resultSet.getInt("user_id"));
        dbOrder.setOffer_id(resultSet.getInt("offer_id"));
        dbOrder.setCalendar_id(resultSet.getInt("calendar_id"));
        dbOrder.setTime(resultSet.getTimestamp("time").toLocalDateTime());
        dbOrder.setCanceled(resultSet.getBoolean("canceled"));
        dbOrder.setCreated_at(resultSet.getTimestamp("created_at").toLocalDateTime());
        dbOrder.setUpdated_at(resultSet.getTimestamp("updated_at").toLocalDateTime());
        return dbOrder;
    }

    public static Calendar getCalendar(ResultSet resultSet) throws SQLException {
        if (!resultSet.next()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }

        Calendar dbCalendar = new Calendar();
        dbCalendar.setId(resultSet.getInt("id"));
        dbCalendar.setMaster_id(resultSet.getInt("master_id"));
        dbCalendar.setStart(resultSet.getTimestamp("offer_begin").toLocalDateTime());
        dbCalendar.setEnd(resultSet.getTimestamp("offer_end").toLocalDateTime());
        return dbCalendar;

    }

    public static Master getMaster(ResultSet resultSet) throws SQLException {
        if (!resultSet.next()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }

        Master dbMaster = new Master();
        dbMaster.setId(resultSet.getInt("id"));
        dbMaster.setEmploy_id(resultSet.getInt("employ_id"));
        dbMaster.setUser_id(resultSet.getInt("user_id"));
        dbMaster.setDescription(resultSet.getString("description"));
        dbMaster.setActive(resultSet.getBoolean("active"));
        dbMaster.setCreated_at(resultSet.getTimestamp("created_at").toLocalDateTime());
        dbMaster.setUpdated_at(resultSet.getTimestamp("updated_at").toLocalDateTime());
        return dbMaster;
    }

    public static Offer getOffer(ResultSet resultSet) throws SQLException {
        if (!resultSet.next()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }

        Offer dbOffer = new Offer();
        dbOffer.setId(resultSet.getInt("id"));
        dbOffer.setMaster_id(resultSet.getInt("master_id"));
        dbOffer.setName(resultSet.getString("name"));
        dbOffer.setPrice(resultSet.getInt("price"));
        return dbOffer;
    }

    public static User getUser(ResultSet resultSet) throws SQLException {
        if (!resultSet.next()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }

        User dbUser = new User(
                resultSet.getString("login"),
                resultSet.getString("password"),
                resultSet.getBoolean("enabled"),
                true,
                true,
                true,
                null
        );
        dbUser.setId(resultSet.getInt("id"));
        return dbUser;
    }
}
