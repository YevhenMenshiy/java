package org.spd.masters.calendar.crud.service;

import java.util.List;
import org.spd.masters.entity.Calendar;

public interface CalendarService {
    Calendar findById(Integer calendarId);

    List<Calendar> get();

    Calendar create(Calendar calendar);

    void update(Integer calendarId, Calendar calendar);

    void remove(Integer calendarId);

    void remove();
}
