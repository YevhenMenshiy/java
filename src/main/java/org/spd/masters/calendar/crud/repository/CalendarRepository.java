package org.spd.masters.calendar.crud.repository;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Calendar;

public interface CalendarRepository {

    Integer insert(Calendar calendar);

    Calendar save(Calendar calendar);

    List<Calendar> findAll();

    Optional<Calendar> findById(Integer id);

    void deleteById(Integer id);

    int updateById(Integer id, Calendar calendar);

    void deleteAll();
}
