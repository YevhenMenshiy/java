package org.spd.masters.calendar.crud.service;

import java.util.List;
import java.util.Optional;
import org.spd.masters.calendar.crud.repository.CalendarRepository;
import org.spd.masters.entity.Calendar;
import org.spd.masters.exceptions.BadRequestException;
import org.spd.masters.exceptions.NotFoundException;
import org.spd.masters.resource.StringResoures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersistentCalendarService implements CalendarService {

    public static final int DONE = 1;

    private final CalendarRepository calendarRepository;

    @Autowired
    public PersistentCalendarService(CalendarRepository calendarRepository) {
        this.calendarRepository = calendarRepository;
    }

    @Override
    public Calendar findById(Integer calendarId) {
        Optional<Calendar> calendar = calendarRepository.findById(calendarId);

        if (calendar.isEmpty()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }
        return calendar.get();
    }

    @Override
    public List<Calendar> get() {

        var calendars = calendarRepository.findAll();
        if (calendars.isEmpty()) {
            throw new NotFoundException(StringResoures.NOT_PRESENT);
        }
        return calendars;
    }

    @Override
    public Calendar create(Calendar calendar) {
        try {
            return calendarRepository.save(calendar);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public void update(Integer calendarId, Calendar calendar) {
        Optional<Calendar> existingCalendar = calendarRepository.findById(calendarId);

        if (existingCalendar.isEmpty()) {
            throw new NotFoundException(StringResoures.CALENDAR_NOT_PRESENT);
        }

        var updated = calendarRepository.updateById(existingCalendar.get().getId(), calendar);

        if (updated != DONE) {
            throw new BadRequestException(StringResoures.NOT_UPDATED);
        }

    }

    @Override
    public void remove(Integer calendarId) {
        Optional<Calendar> calendar = calendarRepository.findById(calendarId);

        if (calendar.isEmpty()) {
            throw new NotFoundException(StringResoures.CALENDAR_NOT_PRESENT);
        }
        calendarRepository.deleteById(calendarId);

    }

    @Override
    public void remove() {
        calendarRepository.deleteAll();
    }
}
