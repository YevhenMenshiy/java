package org.spd.masters.calendar.crud.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.spd.masters.entity.Calendar;
import org.spd.masters.util.DbEntityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository("CalendarRepository")
public class JdbcCalendarRepository implements CalendarRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcCalendarRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Integer insert(Calendar calendar) {

        SqlParameterSource toCalendar =
                new MapSqlParameterSource()
                        .addValue("master_id", calendar.getMaster_id())
                        .addValue("start", calendar.getStart())
                        .addValue("end", calendar.getEnd());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO calendars (master_id, start, end) " +
                "VALUES (:master_id, :start, :end)";

        try {
            jdbcTemplate.update(sql, toCalendar, keyHolder);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        final Map<String, Object> map = keyHolder.getKeys();

        if (map == null) {
            throw new RuntimeException("Calendar not inserted to database!");
        }

        return (Integer) map.get("id");
    }

    public Calendar save(Calendar calendar) {
        int calendarId = insert(calendar);
        calendar.setId(calendarId);
        return calendar;
    }

    @Override
    public List<Calendar> findAll() {
        return jdbcTemplate
                .query("SELECT * FROM calendars",
                        (resultSet, i) -> DbEntityUtil.getCalendar(resultSet)
                );
    }

    @Override
    public Optional<Calendar> findById(Integer id) {
        try {
            Calendar calendar = (Calendar) jdbcTemplate
                    .queryForObject("SELECT * FROM calendars WHERE id = :id",
                            new MapSqlParameterSource("id", id),
                            (resultSet, i) -> DbEntityUtil.getCalendar(resultSet)
                    );
            return Optional.ofNullable(calendar);
        } catch (EmptyResultDataAccessException exception) {
            return Optional.empty();
        }
    }

    @Override
    public void deleteById(Integer id) {
        jdbcTemplate
                .update("DELETE FROM calendars WHERE id = :id",
                        new MapSqlParameterSource("id", id)
                );
    }

    @Override
    public int updateById(Integer id, Calendar calendar) {
        SqlParameterSource toCalendar =
                new MapSqlParameterSource("id", id)
                        .addValue("master_id", calendar.getMaster_id())
                        .addValue("start", calendar.getStart())
                        .addValue("end", calendar.getEnd());

        String sql = "UPDATE calendars SET " +
                "master_id = :master_id, start = :start, end = :end WHERE id = :id";
        return jdbcTemplate.update(sql, toCalendar);
    }

    @Override
    public void deleteAll() {
        jdbcTemplate.update("DELETE FROM calendars",
                new MapSqlParameterSource());
    }

}
