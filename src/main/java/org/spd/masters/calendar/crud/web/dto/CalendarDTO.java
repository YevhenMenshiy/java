package org.spd.masters.calendar.crud.web.dto;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;

public class CalendarDTO {

    @NotNull(message = "Master id can`t be empty")
    Integer master_id;

    @NotNull(message = "Work time start can`t be empty")
    LocalDateTime start;

    @NotNull(message = "Work time end datetime can`t be empty")
    LocalDateTime end;

    public Integer getMaster_id() {
        return master_id;
    }

    public void setMaster_id(Integer masterId) {
        this.master_id = masterId;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }
}
