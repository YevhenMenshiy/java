package org.spd.masters.calendar.crud.web.controller;

import javax.validation.Valid;
import org.spd.masters.calendar.crud.service.CalendarService;
import org.spd.masters.calendar.crud.web.dto.CalendarDTO;
import org.spd.masters.entity.Calendar;
import org.spd.masters.resource.StringResoures;
import org.spd.masters.util.CustomResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/crud/calendars")
public class CalendarCRUDController {

    private final CalendarService calendarService;

    @Autowired
    public CalendarCRUDController(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CustomResponse getAll() {
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK, calendarService.get());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CustomResponse create(@Valid @RequestBody CalendarDTO dto) {
        var saved = calendarService.create(toEntity(dto));
        return new CustomResponse(StringResoures.CREATED_SUCCESSFUL, HttpStatus.CREATED, saved);
    }

    @GetMapping("/{calendarId}")
    public CustomResponse getById(@PathVariable Integer calendarId) {
        var response = calendarService.findById(calendarId);
        return new CustomResponse(StringResoures.SUCCESSFUL, HttpStatus.OK, response);
    }

    @PutMapping("/{calendarId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse update(@PathVariable Integer calendarId, @RequestBody CalendarDTO dto) {
        var saved = toEntity(dto);
        calendarService.update(calendarId, toEntity(dto));
        return new CustomResponse(StringResoures.UPDATED_SUCCESSFUL, HttpStatus.CREATED, saved);
    }

    @DeleteMapping("/{calendarId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteById(@PathVariable Integer calendarId) {
        calendarService.remove(calendarId);
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CustomResponse deleteAll() {
        calendarService.remove();
        return new CustomResponse(StringResoures.DELETED_SUCCESSFUL, HttpStatus.ACCEPTED);
    }

    private Calendar toEntity(CalendarDTO dto) {
        Calendar calendar = new Calendar();
        BeanUtils.copyProperties(dto, calendar);
        return calendar;
    }
}
