package org.spd.masters.calendar.available.repositoty;

import java.util.List;
import org.spd.masters.entity.Calendar;

public interface CalendarRepository {
    List<Calendar> getAllFuture();
}
