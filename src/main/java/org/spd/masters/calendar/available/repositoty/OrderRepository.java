package org.spd.masters.calendar.available.repositoty;

import java.util.List;
import org.spd.masters.entity.Order;

public interface OrderRepository {
    List<Order> getOrdersByCalendarId(Integer calendar_id);
}
