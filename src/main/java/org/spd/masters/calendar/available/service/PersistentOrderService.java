package org.spd.masters.calendar.available.service;

import java.util.ArrayList;
import java.util.List;
import org.spd.masters.calendar.available.repositoty.OrderRepository;
import org.spd.masters.entity.Calendar;
import org.spd.masters.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("OrderService")
public class PersistentOrderService implements OrderService {
    private final OrderRepository orderRepository;

    @Autowired
    public PersistentOrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Order> getOrdersByCalendars(List<Calendar> calendars) {
        List<Order> orders = new ArrayList<>();
        for (Calendar calendar : calendars) {
            orders.addAll(orderRepository.getOrdersByCalendarId(calendar.getId()));
        }

        return orders;
    }
}
