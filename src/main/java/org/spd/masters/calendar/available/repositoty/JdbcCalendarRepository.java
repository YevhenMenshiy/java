package org.spd.masters.calendar.available.repositoty;

import java.util.List;
import org.spd.masters.entity.Calendar;
import org.spd.masters.util.DbEntityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcCalendarRepository implements CalendarRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcCalendarRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Calendar> getAllFuture() {
        return jdbcTemplate
                .query("SELECT * FROM calendars WHERE offer_end > NOW()",
                        (resultSet, i) -> DbEntityUtil.getCalendar(resultSet)
                );
    }


}
