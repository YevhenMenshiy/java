package org.spd.masters.calendar.available.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.spd.masters.calendar.available.repositoty.CalendarRepository;
import org.spd.masters.entity.Calendar;
import org.spd.masters.entity.Order;
import org.spd.masters.entity.Slot;
import org.spd.masters.exceptions.NotFoundException;
import org.spd.masters.resource.StringResoures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrepareSlotsService {

    private final CalendarRepository calendarRepository;
    private final OrderService orderService;

    @Autowired
    public PrepareSlotsService(CalendarRepository calendarRepository,
                               OrderService orderService) {
        this.calendarRepository = calendarRepository;
        this.orderService = orderService;
    }

    public List<Slot> getAvailableSlots() {
        List<Calendar> calendars = calendarRepository.getAllFuture();
        checkCalendars(calendars);

        return removeOrdered(createSlotsForCalendars(calendars),
                orderService.getOrdersByCalendars(calendars));
    }

    List<Slot> createSlotsForCalendars(List<Calendar> calendars) {
        List<Slot> slots = new ArrayList<>();

        checkCalendars(calendars);

        for (Calendar calendar : calendars) {
            List<Slot> slotsForCalendar = createSlotsForCalendar(calendar);
            slots.addAll(slotsForCalendar);
        }
        return slots;
    }

    private void checkCalendars(List<Calendar> calendars) {

        if (calendars == null || calendars.isEmpty()) {
            throw new NotFoundException(StringResoures.NO_CALENDARS);
        }
    }

    private List<Slot> createSlotsForCalendar(Calendar calendar) {
        return fillSlots(calendar.getId(), calendar.getStart(), calendar.getEnd());
    }

    List<Slot> fillSlots(Integer calendarId, LocalDateTime startTime, LocalDateTime endTime) {
        List<Slot> slots = new ArrayList<>();

        LocalDateTime time = startTime;

        if (time.toLocalDate().isBefore(LocalDate.now())) {
            return slots;
        }

        if (time.toLocalDate().equals(LocalDate.now())
                && time.toLocalTime().isBefore(LocalTime.now())) {

            time = LocalDateTime.of(time.toLocalDate(),
                    LocalTime.now().truncatedTo(ChronoUnit.HOURS).plusHours(Slot.SLOT_TIME));
        }

        do {
            slots.add(new Slot(calendarId, time));
            time = time.plusHours(Slot.SLOT_TIME);
        } while (!time.equals(endTime));

        return slots;
    }

    List<Slot> removeOrdered(List<Slot> slots, List<Order> orders) {

        if (slots == null || slots.isEmpty()) {
            throw new NotFoundException(StringResoures.NO_SLOTS);
        }

        if (orders == null || orders.isEmpty()) {
            return slots;
        }

        Set<LocalDateTime> orderedTime = orders.stream()
                .map(Order::getTime)
                .collect(Collectors.toSet());

        return slots.stream()
                .filter(s -> !orderedTime.contains(s.getStartTime()))
                .collect(Collectors.toList());
    }
}
