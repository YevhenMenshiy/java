package org.spd.masters.calendar.available.service;

import java.time.LocalDate;
import java.util.List;
import org.spd.masters.entity.Company;
import org.spd.masters.entity.Master;
import org.spd.masters.entity.Slot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersistentAvailableSlotsService implements AvailableSlotsService {

    private final PrepareSlotsService slotService;

    @Autowired
    public PersistentAvailableSlotsService(PrepareSlotsService slotService) {
        this.slotService = slotService;
    }

    @Override
    public List<Slot> getAllAvailable() {
        return slotService.getAvailableSlots();
    }

    @Override
    public List<Slot> getAvailableOnDate(LocalDate date) {
        return null;
    }

    @Override
    public List<Slot> getAllAvailableByCompany(Company company) {
        return null;
    }

    @Override
    public List<Slot> getAvailableByCompanyOnDate(Company company, LocalDate date) {
        return null;
    }

    @Override
    public List<Slot> getAllAvailableByMaster(Master master) {
        return null;
    }

    @Override
    public List<Slot> getAvailableByMasterOnDate(Master master, LocalDate date) {
        return null;
    }
}
