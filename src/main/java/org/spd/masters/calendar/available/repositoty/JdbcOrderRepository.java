package org.spd.masters.calendar.available.repositoty;

import java.util.List;
import org.spd.masters.entity.Order;
import org.spd.masters.util.DbEntityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("OrderRepository")
public class JdbcOrderRepository implements OrderRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcOrderRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Order> getOrdersByCalendarId(Integer calendar_id) {
        return jdbcTemplate
                .query("SELECT * FROM orders WHERE calendar_id = " + calendar_id,
                        (resultSet, i) -> DbEntityUtil.getOrder(resultSet)
                );
    }
}
