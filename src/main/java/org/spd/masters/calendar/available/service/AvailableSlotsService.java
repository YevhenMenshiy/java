package org.spd.masters.calendar.available.service;

import java.time.LocalDate;
import java.util.List;
import org.spd.masters.entity.Company;
import org.spd.masters.entity.Master;
import org.spd.masters.entity.Slot;
import org.spd.masters.exceptions.NotFoundException;

public interface AvailableSlotsService {
    List<Slot> getAllAvailable();

    List<Slot> getAvailableOnDate(LocalDate date);

    List<Slot> getAllAvailableByCompany(Company company);

    List<Slot> getAvailableByCompanyOnDate(Company company, LocalDate date);

    List<Slot> getAllAvailableByMaster(Master master);

    List<Slot> getAvailableByMasterOnDate(Master master, LocalDate date);
}
