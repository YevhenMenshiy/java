package org.spd.masters.calendar.available.web.controller;

import java.util.List;
import org.spd.masters.calendar.available.service.AvailableSlotsService;
import org.spd.masters.entity.Slot;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/slots")
public class SlotController {
    private final AvailableSlotsService availableSlotsService;

    public SlotController(AvailableSlotsService availableSlotsService) {
        this.availableSlotsService = availableSlotsService;
    }

    @GetMapping("/available")
    public ResponseEntity<List<Slot>> getAvailable() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(availableSlotsService.getAllAvailable());
    }
}
