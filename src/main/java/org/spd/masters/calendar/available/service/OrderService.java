package org.spd.masters.calendar.available.service;

import java.util.List;
import org.spd.masters.entity.Calendar;
import org.spd.masters.entity.Order;
import org.spd.masters.entity.Slot;

public interface OrderService {
    List<Order> getOrdersByCalendars(List<Calendar> calendars);
}
