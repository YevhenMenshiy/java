package org.spd.masters.company.outofbusiness.repository;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Order;
import org.spd.masters.entity.User;

public interface CompanyOrdersRepository {

    int closeById(int id);

    List<Order> getUpcomingOrders(int id);

    User getOrderUser(int id);
}
