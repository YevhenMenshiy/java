package org.spd.masters.company.outofbusiness.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.spd.masters.entity.Order;
import org.spd.masters.entity.User;
import org.spd.masters.util.DbEntityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcCompanyOrdersRepository implements CompanyOrdersRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcCompanyOrdersRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int closeById(int id) {
        return jdbcTemplate
                .update("UPDATE companies SET closed = :closed WHERE id = :id",
                        new MapSqlParameterSource("id", id)
                                .addValue("closed", true)
                );
    }

    @Override
    public List<Order> getUpcomingOrders(int id) {
        return jdbcTemplate
                .query("SELECT * FROM orders WHERE time > NOW() AND WHERE offer_id = " +
                                "(SELECT id FROM offers WHERE master_id = " +
                                "(SELECT id FROM employers WHERE company_id = :id))",
                        new MapSqlParameterSource("id", id),
                        (resultSet, i) -> DbEntityUtil.getOrder(resultSet)
                );
    }

    @Override
    public User getOrderUser(int id) {
        return jdbcTemplate
                .queryForObject(
                        "SELECT * FROM users WHERE id = " +
                                "(SELECT client_user_id FROM orders WHERE id = :id)",
                        new MapSqlParameterSource("id", id),
                        (resultSet, i) -> DbEntityUtil.getUser(resultSet)
                );
    }

}
