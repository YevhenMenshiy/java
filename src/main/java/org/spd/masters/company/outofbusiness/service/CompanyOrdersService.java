package org.spd.masters.company.outofbusiness.service;

import java.util.List;
import org.spd.masters.entity.Order;
import org.spd.masters.entity.User;

public interface CompanyOrdersService {

    boolean deleteById(int id);

    List<Order> getUpcomingOrders(int id);

    User getOrderUser(int id);
}
