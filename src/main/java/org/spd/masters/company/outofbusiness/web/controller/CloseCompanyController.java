package org.spd.masters.company.outofbusiness.web.controller;

import org.spd.masters.company.outofbusiness.service.CompanyOrdersService;
import org.spd.masters.company.outofbusiness.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CloseCompanyController {

    private final CompanyOrdersService persistentCompanyOrdersService;

    @Autowired
    public CloseCompanyController(CompanyOrdersService persistentCompanyService,
                                  MailService mailService) {
        this.persistentCompanyOrdersService = persistentCompanyService;
    }

    @DeleteMapping("/api/v1/company-close/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        HttpStatus status = persistentCompanyOrdersService.deleteById(id)
                ? HttpStatus.ACCEPTED
                : HttpStatus.NOT_FOUND;

        return new ResponseEntity<>(status);
    }
}
