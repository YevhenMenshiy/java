package org.spd.masters.company.outofbusiness.service;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spd.masters.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailService {

    private final Logger logger = LoggerFactory.getLogger(MailService.class);

    private final JavaMailSender mailSender;
    private final Counter counter;

    private final ExecutorService emailExecutor;

    @Autowired
    public MailService(JavaMailSender mailSender,
                       Counter counter) {
        this.mailSender = mailSender;
        this.counter = counter;

        this.emailExecutor = Executors.newFixedThreadPool(3);
    }

    public void notifyAboutOutOfBusiness(List<User> clients) {

        for (User client : clients) {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setSubject("Appointment canceled!");
            message.setText("Your appointment canceled because company out of business");
            message.setTo(client.getEmail());
            message.setFrom("no-reply@masters.com.ua");

            emailExecutor.execute(() -> {
                mailSender.send(message);
                counter.increment();
            });
        }

        destroy();

        logger.info("Sent emails count is: " + counter.getCount());
    }

    @PreDestroy
    public void destroy() {
        emailExecutor.shutdown();
    }
}

