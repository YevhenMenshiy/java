package org.spd.masters.company.outofbusiness.service;


import java.util.ArrayList;
import java.util.List;
import org.spd.masters.company.outofbusiness.repository.CompanyOrdersRepository;
import org.spd.masters.entity.Order;
import org.spd.masters.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersistentCompanyOrdersService implements CompanyOrdersService {
    private final CompanyOrdersRepository companyOrdersRepository;
    private final MailService mailService;

    @Autowired
    public PersistentCompanyOrdersService(CompanyOrdersRepository companyRepository,
                                          MailService mailService) {
        this.companyOrdersRepository = companyRepository;
        this.mailService = mailService;
    }

    public boolean deleteById(int id) {
        notifyClients(id);

        return companyOrdersRepository.closeById(id) == 1;
    }

    private void notifyClients(int id) {
        List<User> clients = new ArrayList<User>();
        for (Order order : getUpcomingOrders(id)) {
            clients.add(getOrderUser(order.getId()));
        }
        mailService.notifyAboutOutOfBusiness(clients);
    }

    @Override
    public List<Order> getUpcomingOrders(int id) {
        return companyOrdersRepository.getUpcomingOrders(id);
    }

    @Override
    public User getOrderUser(int id) {
        return companyOrdersRepository.getOrderUser(id);
    }

}
