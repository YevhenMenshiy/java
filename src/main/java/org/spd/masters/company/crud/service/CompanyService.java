package org.spd.masters.company.crud.service;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Company;

public interface CompanyService {
    int create(Company company);

    List<Company> getAll();

    Optional<Company> getById(int id);

    boolean deleteById(int id);

    int updateById(int id, Company company);

    void deleteAll();
}
