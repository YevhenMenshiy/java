package org.spd.masters.company.crud.web.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CompanyDTO {

    @NotEmpty(message = "Name can`t be empty")
    @Size(min = 3, max = 150)
    private String name;

    @NotEmpty(message = "Address can`t be empty")
    @Size(min = 3, max = 255)
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
