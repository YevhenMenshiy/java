package org.spd.masters.company.crud.repository;

import java.util.List;
import java.util.Optional;
import org.spd.masters.entity.Company;

public interface CompanyRepository {

    int insert(Company company);

    List<Company> findAll();

    Optional<Company> findById(int id);

    int updateById(int id, Company company);

    int deleteById(int id);

    void deleteAll();
}
