package org.spd.masters.company.crud.service;

import java.util.List;
import java.util.Optional;
import org.spd.masters.company.crud.repository.CompanyRepository;
import org.spd.masters.entity.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersistentCompanyService implements CompanyService {
    private final CompanyRepository companyRepository;

    @Autowired
    public PersistentCompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public int create(Company company) {
        return companyRepository.insert(company);
    }

    public List<Company> getAll() {
        return companyRepository.findAll();
    }

    public Optional<Company> getById(int id) {
        return companyRepository.findById(id);
    }

    public boolean deleteById(int id) {
        return companyRepository.deleteById(id) == 1;
    }

    public int updateById(int id, Company company) {
        return companyRepository.updateById(id, company);
    }

    public void deleteAll() {
        companyRepository.deleteAll();
    }
}
