package org.spd.masters.company.crud.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.spd.masters.entity.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcCompanyRepository implements CompanyRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcCompanyRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insert(Company company) {
        SqlParameterSource toCompany =
                new MapSqlParameterSource()
                        .addValue("name", company.getName())
                        .addValue("address", company.getAddress());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate
                .update("INSERT INTO companies (name, address, created_at) values (:name, :address, NOW())",
                        toCompany, keyHolder);
        final Map<String, Object> map = keyHolder.getKeys();

        if (map == null) {
            throw new RuntimeException("Company not inserted to database!");
        }

        return (int) map.get("id");
    }

    @Override
    public List<Company> findAll() {
        return jdbcTemplate
                .query("SELECT * FROM companies",
                        (resultSet, i) -> getCompany(resultSet)
                );
    }

    @Override
    public Optional<Company> findById(int id) {
        try {
            Company company = (Company) jdbcTemplate
                    .queryForObject("SELECT * FROM companies WHERE id = :id",
                            new MapSqlParameterSource("id", id),
                            (resultSet, i) -> getCompany(resultSet)
                    );
            return Optional.ofNullable(company);
        } catch (EmptyResultDataAccessException exception) {
            return Optional.empty();
        }
    }

    @Override
    public int deleteById(int id) {
        return jdbcTemplate
                .update("DELETE FROM companies WHERE id = :id",
                        new MapSqlParameterSource("id", id)
                );
    }

    @Override
    public int updateById(int id, Company company) {
        SqlParameterSource toCompany =
                new MapSqlParameterSource("id", id)
                        .addValue("name", company.getName())
                        .addValue("address", company.getAddress());

        return jdbcTemplate
                .update("UPDATE companies SET name = :name, address = :address, updated_at = NOW() WHERE id = :id",
                        toCompany);
    }

    @Override
    public void deleteAll() {
        jdbcTemplate
                .update("DELETE FROM companies",
                        new MapSqlParameterSource());
    }

    private Company getCompany(ResultSet resultSet) throws SQLException {
        Company dbCompany = new Company();
        dbCompany.setId(resultSet.getInt("id"));
        dbCompany.setName(resultSet.getString("name"));
        dbCompany.setAddress(resultSet.getString("address"));
        return dbCompany;
    }
}
