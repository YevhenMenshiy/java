package org.spd.masters.company.crud.web.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spd.masters.company.crud.service.PersistentCompanyService;
import org.spd.masters.company.crud.web.dto.CompanyDTO;
import org.spd.masters.entity.Company;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/crud/companies")
public class CompanyCRUDController {

    private final PersistentCompanyService persistentCompanyService;
    private final Logger logger = LoggerFactory.getLogger(CompanyCRUDController.class);

    @Autowired
    public CompanyCRUDController(PersistentCompanyService persistentCompanyService) {
        this.persistentCompanyService = persistentCompanyService;
    }

    @GetMapping
    public ResponseEntity<List<Company>> getAll() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(persistentCompanyService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable int id) {
        Optional<Company> company = persistentCompanyService.getById(id);

        HttpStatus status = company.isEmpty()
                ? HttpStatus.NOT_FOUND
                : HttpStatus.OK;

        return ResponseEntity
                .status(status)
                .body(company);
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody CompanyDTO dto) {
        Company company = toEntity(dto);
        int id = persistentCompanyService.create(company);
        company.setId(id);
        return new ResponseEntity<>(company, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable int id, @Valid @RequestBody CompanyDTO dto) {
        if (persistentCompanyService.getById(id).isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Company company = toEntity(dto);
        persistentCompanyService.updateById(id, company);

        company.setId(id);
        return new ResponseEntity<>(company, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        boolean isRemoved = persistentCompanyService.deleteById(id);

        HttpStatus status = isRemoved
                ? HttpStatus.ACCEPTED
                : HttpStatus.NOT_FOUND;

        return new ResponseEntity<>(status);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteAll() {
        persistentCompanyService.deleteAll();
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    private Company toEntity(CompanyDTO dto) {
        Company company = new Company();
        BeanUtils.copyProperties(dto, company);
        return company;
    }
}
