package org.spd.masters.entity;

import java.time.LocalDateTime;

public class Calendar {

    private Integer id;

    private Integer master_id;

    private LocalDateTime start;

    private LocalDateTime end;

    public Calendar(Integer id, Integer masterId, LocalDateTime start, LocalDateTime end) {
        this.id = id;
        this.master_id = masterId;
        this.start = start;
        this.end = end;
    }

    public Calendar() {

    }


    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public Integer getMaster_id() {
        return master_id;
    }

    public void setMaster_id(Integer masterId) {
        this.master_id = masterId;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "Calendar{" +
                "id=" + id +
                ", master_id=" + master_id +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}
