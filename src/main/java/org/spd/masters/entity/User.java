package org.spd.masters.entity;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

public class User extends org.springframework.security.core.userdetails.User {

    private Integer id;

    private String login;

    private String password;

    private Boolean enabled;

    public User(String login, String password,
                Collection<? extends GrantedAuthority> authorities) {
        super(login, password, authorities);
    }

    public User(String login, String password, boolean enabled, boolean accountNonExpired,
                boolean credentialsNonExpired, boolean accountNonLocked,
                Collection<? extends GrantedAuthority> authorities) {
        super(login, password, enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked,
                authorities);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return login;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
