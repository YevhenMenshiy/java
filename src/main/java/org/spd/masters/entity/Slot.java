package org.spd.masters.entity;

import java.time.LocalDateTime;

public class Slot {

    public final static Integer SLOT_TIME = 1;

    Integer calendar_id;
    LocalDateTime startTime;

    public Slot(Integer id, LocalDateTime time) {
        this.calendar_id = id;
        this.startTime = time;
    }

    public Integer getCalendar_id() {
        return calendar_id;
    }

    public void setCalendar_id(Integer calendar_id) {
        this.calendar_id = calendar_id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Slot slot = (Slot) o;

        if (!calendar_id.equals(slot.calendar_id)) {
            return false;
        }
        return startTime.equals(slot.startTime);
    }

    @Override
    public int hashCode() {
        int result = calendar_id.hashCode();
        result = 31 * result + startTime.hashCode();
        return result;
    }


    @Override
    public String toString() {
        return "Slot{" +
                "SLOT_TIME=" + SLOT_TIME +
                ", calendar_id=" + calendar_id +
                ", startTime=" + startTime +
                '}';
    }
}
