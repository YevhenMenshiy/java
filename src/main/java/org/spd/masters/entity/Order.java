package org.spd.masters.entity;

import java.time.LocalDateTime;

public class Order {

    Integer id;
    Integer user_id;
    Integer offer_id;
    Integer calendar_id;
    LocalDateTime time;
    Boolean canceled;
    LocalDateTime created_at;
    LocalDateTime updated_at;

    public Order(LocalDateTime time) {
        this.time = time;
    }

    public Order() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer orderId) {
        this.id = orderId;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(Integer offer_id) {
        this.offer_id = offer_id;
    }

    public Integer getCalendar_id() {
        return calendar_id;
    }

    public void setCalendar_id(Integer calendar_id) {
        this.calendar_id = calendar_id;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public Boolean getCanceled() {
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }

    public LocalDateTime getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", offer_id=" + offer_id +
                ", calendar_id=" + calendar_id +
                ", time=" + time +
                ", canceled=" + canceled +
                ", created_at=" + created_at +
                ", updated_at=" + updated_at +
                '}';
    }
}
