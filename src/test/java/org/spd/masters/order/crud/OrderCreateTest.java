package org.spd.masters.order.crud;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.spd.masters.config.TestDbConfig;
import org.spd.masters.util.CustomResponse;
import org.spd.masters.order.crud.web.dto.OrderDTO;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("test")
@ContextConfiguration(classes = {TestDbConfig.class})
@Sql({"classpath:drop.sql", "classpath:test-data.sql"})
public class OrderCreateTest {

    private static final String LOGIN = "admin";
    private static final String PASSWORD = "admin";

    @LocalServerPort
    private int testPort;

    private final TestRestTemplate rest = new TestRestTemplate();

    @Test
    @DisplayName("Create Order")
    void create() {
        OrderDTO dto = new OrderDTO();
        dto.setUser_id(2);
        dto.setCalendar_id(1);
        dto.setTime(LocalDateTime.now().truncatedTo(ChronoUnit.HOURS));

        String url = "http://localhost:" + testPort + "/api/v1/crud/orders";

        final ResponseEntity<CustomResponse> response = rest
                .withBasicAuth(LOGIN, PASSWORD)
                .postForEntity(url, dto, CustomResponse.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.CREATED);

//        assertThat(((Map<?, ?>) response.getBody().getResponse()).values().toArray()[0]).isEqualTo(1);

    }
}
