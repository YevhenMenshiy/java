package org.spd.masters.order.crud.web.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.spd.masters.entity.Order;
import org.spd.masters.order.crud.service.PersistentOrderService;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
@WithMockUser(roles = "ADMIN")
class OrderCRUDControllerTest {

    @InjectMocks
    private OrderCRUDController controller;

    @Mock
    private PersistentOrderService service;

    MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void validCreateResult() throws Exception {

        final var request = "{\"user_id\":2, \"calendar_id\":1, \"time\":\"" +
                LocalDateTime.now().truncatedTo(ChronoUnit.HOURS) + "\"}";

        when(service.create(any())).thenReturn(new Order());

        mvc.perform(post("/api/v1/crud/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isCreated());
    }
}