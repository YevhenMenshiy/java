package org.spd.masters.calendar.available.service;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.spd.masters.config.TestDbConfig;
import org.spd.masters.entity.Calendar;
import org.spd.masters.entity.Order;
import org.spd.masters.entity.Slot;
import org.spd.masters.exceptions.NotFoundException;
import org.spd.masters.resource.StringResoures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@Disabled //todo fix & remove
@SpringBootTest
@ContextConfiguration(classes = {TestDbConfig.class})
@ActiveProfiles("test")
class PrepareSlotsServiceTest {

    @Autowired
    PrepareSlotsService slotService;

    @Test
    void fillSlotsTest() {
        var slots = List.of(
                new Slot(1, LocalDateTime.of(LocalDate.now(),
                        LocalTime.now().plusHours(1).truncatedTo(ChronoUnit.HOURS))),
                new Slot(1, LocalDateTime.of(LocalDate.now(),
                        LocalTime.now().plusHours(2).truncatedTo(ChronoUnit.HOURS)))
        );

        var slotsList = slotService.fillSlots(
                1,
                LocalDateTime.of(LocalDate.now(), LocalTime.now().truncatedTo(ChronoUnit.HOURS)),
                LocalDateTime.of(LocalDate.now(),
                        LocalTime.now().plusHours(3).truncatedTo(ChronoUnit.HOURS))
        );

        assertEquals(slots, slotsList);
    }

    @Test
    void fillSlotsFromDaysBeforeTodayIsEmptyTest() {
        var slotsList = slotService.fillSlots(
                1,
                LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.parse("09:00")),
                LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.parse("18:00"))
        );

        assertEquals(new ArrayList<>(), slotsList);
    }

    @Test
    void fillSlotsFromTimeBeforeNowWillClearTest() {
        var slots = List.of(
                new Slot(1, LocalDateTime.of(LocalDate.now(),
                        LocalTime.now().plusHours(1).truncatedTo(ChronoUnit.HOURS))),
                new Slot(1, LocalDateTime.of(LocalDate.now(),
                        LocalTime.now().plusHours(2).truncatedTo(ChronoUnit.HOURS)))
        );

        var slotsList = slotService.fillSlots(
                1,
                LocalDateTime.of(LocalDate.now(),
                        LocalTime.now().minusHours(3).truncatedTo(ChronoUnit.HOURS)),
                LocalDateTime.of(LocalDate.now(),
                        LocalTime.now().plusHours(3).truncatedTo(ChronoUnit.HOURS))
        );

        assertEquals(slots, slotsList);
    }

    @Test
    void removeOrderedTest() throws NotFoundException {
        var slotsList = slotService.fillSlots(
                1,
                LocalDateTime.of(LocalDate.now(), LocalTime.parse("09:00")),
                LocalDateTime.of(LocalDate.now(), LocalTime.parse("18:00"))
        );

        var slots = slotService.fillSlots(
                1,
                LocalDateTime.of(LocalDate.now(), LocalTime.parse("09:00")),
                LocalDateTime.of(LocalDate.now(), LocalTime.parse("17:00"))
        );

        var orders = List.of(
                new Order(LocalDateTime.of(LocalDate.now(), LocalTime.parse("09:00"))),
                new Order(LocalDateTime.of(LocalDate.now(), LocalTime.parse("17:00")))
        );

        var availableSlots = slotService.removeOrdered(slotsList, orders);

        assertEquals(slots, availableSlots);
    }

    @Test
    void removeOrderedNoOrdersTest() throws NotFoundException {
        var slotsList = slotService.fillSlots(
                1,
                LocalDateTime.of(LocalDate.now(), LocalTime.parse("09:00")),
                LocalDateTime.of(LocalDate.now(), LocalTime.parse("18:00"))
        );

        List<Order> orders = new ArrayList<>();

        var availableSlots = slotService.removeOrdered(slotsList, orders);

        assertEquals(slotsList, availableSlots);
    }

    @Test
    void removeOrderedNullSlotsTestThrowException() throws NotFoundException {

        Exception exception = assertThrows(NotFoundException.class, () -> {
            var availableSlots = slotService.removeOrdered(null, null);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(StringResoures.NO_SLOTS));
    }

    @Test
    void removeOrderedEmptySlotsTestThrowException() throws NotFoundException {

        Exception exception = assertThrows(NotFoundException.class, () -> {
            var availableSlots = slotService.removeOrdered(new ArrayList<>(), null);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(StringResoures.NO_SLOTS));
    }

    @Test
    void createSlotsForCalendarsTest() throws NotFoundException {
        var calendars = List.of(
                new Calendar(1, 1,
                        LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.now().minusHours(3).truncatedTo(ChronoUnit.HOURS)),
                        LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.now().plusHours(3).truncatedTo(ChronoUnit.HOURS))),
                new Calendar(2, 1,
                        LocalDateTime.of(LocalDate.now(), LocalTime.now().minusHours(3).truncatedTo(ChronoUnit.HOURS)),
                        LocalDateTime.of(LocalDate.now(), LocalTime.now().plusHours(3).truncatedTo(ChronoUnit.HOURS))),
                new Calendar(3, 1,
                        LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.now().minusHours(3).truncatedTo(ChronoUnit.HOURS)),
                        LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.now().plusHours(3).truncatedTo(ChronoUnit.HOURS)))
        );

        var slotsFromCalendars = slotService.createSlotsForCalendars(calendars);

        var slotsList = slotService.fillSlots(
                2,
                LocalDateTime.of(LocalDate.now(), LocalTime.now().truncatedTo(ChronoUnit.HOURS)),
                LocalDateTime.of(LocalDate.now(), LocalTime.now().plusHours(3).truncatedTo(ChronoUnit.HOURS))
        );

        slotsList.addAll(
                slotService.fillSlots(
                        3,
                        LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.now().minusHours(3).truncatedTo(ChronoUnit.HOURS)),
                        LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.now().plusHours(3).truncatedTo(ChronoUnit.HOURS))
                )
        );

        assertEquals(slotsFromCalendars, slotsList);
    }

}