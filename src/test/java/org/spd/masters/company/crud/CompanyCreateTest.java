package org.spd.masters.company.crud;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.spd.masters.company.crud.web.dto.CompanyDTO;
import org.spd.masters.config.TestDbConfig;
import org.spd.masters.entity.Company;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ContextConfiguration(classes = {TestDbConfig.class})
@ActiveProfiles("test")
@Sql({"classpath:drop.sql"})
public class CompanyCreateTest {

    private static final String LOGIN = "admin";
    private static final String PASSWORD = "admin";

    @LocalServerPort
    private int testPort;

    private final TestRestTemplate rest = new TestRestTemplate();

    @Test
    @DisplayName("Create barbershop company with name & address")
    void create() {
        CompanyDTO dto = new CompanyDTO();
        dto.setName("Awesome Barbershop");
        dto.setAddress("Pupkina 777");

        String url = "http://localhost:" + testPort + "/api/v1/crud/companies";

        final ResponseEntity<Company> response = rest
                .withBasicAuth(LOGIN, PASSWORD)
                .postForEntity(url, dto, Company.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody().getId()).isEqualTo(1);

    }
}
