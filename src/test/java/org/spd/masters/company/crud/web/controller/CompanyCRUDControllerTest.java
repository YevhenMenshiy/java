package org.spd.masters.company.crud.web.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.spd.masters.company.crud.service.PersistentCompanyService;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WithMockUser(roles = "ADMIN")
class CompanyCRUDControllerTest {

    @Mock
    private PersistentCompanyService service;

    @InjectMocks
    private CompanyCRUDController controller;

    MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void validCreateResultWithId() throws Exception {

        when(service.create(any())).thenReturn(1);

        final var request = "{\"name\":\"Company Name\", \"address\":\"Pushkina 10\"}";

        mvc.perform(post("/api/v1/crud/companies").contentType(MediaType.APPLICATION_JSON).content(request))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    void companyNameCanNotBeNull() throws Exception {

        final var request = "{\"name\":null, \"address\":\"Pushkina 10\"}";

        mvc.perform(post("/api/v1/crud/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    void companyAddressCanNotBeNull() throws Exception {

        final var request = "{\"name\":\"Company\", \"address\":null}";

        mvc.perform(post("/api/v1/crud/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest());
    }
}
