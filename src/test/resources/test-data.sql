INSERT INTO users (id, login, password, enabled) values (1, 'master', 'password', true);
INSERT INTO users (id, login, password, enabled) values (2, 'client', 'password', true);
INSERT INTO companies (id, name, address, created_at) values (1, 'name', 'address', '2022-03-25T09:00');
INSERT INTO roles (id, name) values (1, 'ADMIN');
INSERT INTO employers (id, company_id, user_id, role_id) values (1, 1, 1, 1);
INSERT INTO masters (id, employ_id, user_id, description, created_at) VALUES (1, 1, 1, '', '2022-03-25T09:00');
INSERT INTO calendars (id, master_id, offer_begin, offer_end) VALUES (1, 1, '2022-03-25T09:00', '2022-03-25T18:00');